from sklearn import tree
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
# %matplotlib inline

titanic_data = pd.read_csv('train.csv')
# print(titanic_data.head(10))
# print(titanic_data.isnull().sum())
X = titanic_data.drop(['PassengerId', 'Survived', 'Name', 'Ticket', 'Cabin'], axis=1)
y = titanic_data.Survived

X = pd.get_dummies(X) # переводит строковые переменные в числовые напримерпол 0 1

X = X.fillna({'Age': X.Age.median()})
# print(X.head(6))
clf = tree.DecisionTreeClassifier()
print(clf.fit(X,y))
